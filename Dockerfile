FROM node:21-alpine

WORKDIR /hapi

COPY package*.json ./

RUN npm ci --only=production

COPY . .

EXPOSE 5000
CMD [ "npm", "start" ]